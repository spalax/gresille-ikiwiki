#!/usr/bin/perl
use strict;
use warnings;
use POSIX;
use Time::Local;

package IkiWiki::Plugin::datetime_cmp;

=head1 NAME

IkiWiki::Plugin::datetime_cmp - A family of pagespec to match pages according
to time of creation or modification.

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::datetime_cmp

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

IkiWiki::Plugin::datetime_cmp - A family of pagespec to match pages according
to time of creation or modification.

See doc/plugins/datetime_cmp.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/datetime_cmp
http://ikiwiki.info/plugins/contrib/datetime_cmp/

=head1 HISTORY

This plugin is an improved version of created_in_future, by the same author.

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2012-2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut


use IkiWiki '3.00';
use DateTime::TimeZone;

sub import {
	hook(type => "needsbuild", id => "datetime_cmp", call => \&needsbuild);
}

# Default value for $now. This variable is global to prevent weird cases where
# time changes between two comparisons supposed to be done at the same time.
# So, $now is the time of compilation.
our $now = DateTime->now(time_zone => 'local');

sub needsbuild (@) {
	my $needsbuild=shift;
	foreach my $page (keys %pagestate) {
		if (exists $pagestate{$page}{datetime_cmp}{nextchange}) {
			if ($pagestate{$page}{datetime_cmp}{nextchange} <= $now->epoch()) {
				# Force a rebuild
				delete $pagestate{$page}{datetime_cmp}{nextchange};
				push @$needsbuild, $pagesources{$page};
			}
			if (exists $pagesources{$page} &&
					grep { $_ eq $pagesources{$page} } @$needsbuild) {
				# Remove state, will be re-added if necessary
				delete $pagestate{$page}{datetime_cmp};
			}
		}
	}
	return $needsbuild;
}

package IkiWiki::PageSpec;

use IkiWiki '3.00';
use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration;
use Date::Parse qw(str2time);

sub parse_duration {
	my $pattern;
	my ($string) = @_;

	# Cannot manage to make %P and %p DateTime::Format::Duration patterns to
	# work. Using a workaround.
	my $negative = (substr($string, 0, 1) eq "-");
	if ($negative) {
		$string = substr($string, 1);
	}

	if (index(substr($string, 1), "-") == -1) {
		$pattern = DateTime::Format::Duration->new(pattern=>"%r");
	} elsif (index($string, ":") == -1) {
		$pattern = DateTime::Format::Duration->new(pattern=>"%F");
	} else {
		$pattern = DateTime::Format::Duration->new(pattern=>"%F %r");
	}
	if ($negative) {
		return $pattern->parse_duration($string)->inverse();
	} else {
		return $pattern->parse_duration($string);
	}
}

sub compare {
	my ($cm, $datetime, $operator, $obj, $delta, $page, $argument, %named) = @_;

	# "today" and "now" are treated the same way
	if ($obj eq "today") {
		$obj = "now";
	}

	# Argument management
	if (($delta eq "_delta") and not ($obj eq "now")) {
		($argument, $delta) = split(/, *([^,]+)$/, $argument);
	} elsif (($obj eq "now") and not ($delta eq "_delta")) {
		$delta = "0-0-0 0:0:0";
		$argument = "";
	} elsif ($delta eq "_delta") {
		$delta = $argument;
		$argument = "";
	} else {
		$delta = "0-0-0 0:0:0";
	}
	$delta = parse_duration($delta);
	# $delta and $argument now contain sensible values:
	# - $delta: time delta, as a DateTime::Duration object
	# - $argument: the argument (page name, absolute date, or nothing) as a string.

	# We want to compare "this" (the page containing the pagespec) and "other"
	# (the thing (date, page) we are comparing "this" to).

	# Calculating "this" time
	my $thistime = undef;
	if ($cm eq "c") {
		$thistime = $IkiWiki::pagectime{$page};
	} else {
		$thistime = $IkiWiki::pagemtime{$page};
	}
	$thistime = DateTime->from_epoch(epoch=>$thistime, time_zone=>'local');

	# Calculating "other" time
	my $othertime = undef;
	if ($obj eq "now") {
		$othertime = $now->epoch();
	} elsif ($obj eq "abs") {
		$othertime = str2time($argument);
		if (not defined($othertime)) {
			error("'$argument' could not be parsed as a time delta.");
		}
	} else { # $obj eq "page"
		if ($cm eq "c") {
			error("Page '$argument' does not exist.") if not exists $IkiWiki::pagectime{$argument};
			$othertime = $IkiWiki::pagectime{$argument};
		} else {
			error("Page '$argument' does not exist.") if not exists $IkiWiki::pagemtime{$argument};
			$othertime = $IkiWiki::pagemtime{$argument};
		}
	}
	$othertime = DateTime->from_epoch(epoch=>$othertime, time_zone=>'local');

	# $thistime and $othertime now contain the relevant time, as a DateTime object.

	# Managing delta
	$othertime += $delta;

	# We will need something that is:
	# - the current date if $datetime == 'date';
	# - the current time if $datetime == 'time'.
	my $todaynow;

	# Are we comparing date or time?
	if ($datetime eq "date") {
		$thistime = $thistime->truncate(to=>'day');
		$othertime = $othertime->truncate(to=>'day');
		$todaynow = $now->clone()->truncate(to=>'day');
	} else {
		$todaynow = $now->clone();
	}

	# When is it needed to rebuild $page?
	# If we are comparing to now
	#     No need to ever rebuild
	# If we are comparing to today
	#     Rebuild needed tomorrow
	# If we are comparing to some page
	#     Rebuild when this page changes
	# If we are comparing to some date or time in the past
	#     No need to ever rebuild
	# If we are comparing to some date or time in the future
	#     Rebuild after this date or time
	if ($obj eq "abs") {
		# No need to update this page
	} elsif ($obj eq "page") {
		add_depends($page, $argument);
	} elsif (($obj eq "now") and (DateTime->compare($thistime, $now)==1)) {
		# Comparing to "now", and page created in the future.
		my $nextchange = $thistime->epoch();
		if (($datetime eq "date") and (($operator eq "gt") or ($operator eq "leq"))) {
			$nextchange += 24*60*60; # Add one day
		}
		if (exists $named{'location'}) {
			my $parent = $named{'location'};
			if (exists $pagestate{$parent}{datetime_cmp}{nextchange}) {
				$pagestate{$parent}{datetime_cmp}{nextchange} = min(
					$pagestate{$parent}{datetime_cmp}{nextchange},
					$nextchange
				);
			} else {
				$pagestate{$parent}{datetime_cmp}{nextchange} = $nextchange;
			}
		}
	}

	# Comparison
	my $result = undef;
	if ($operator eq "eq") {
		$result = (DateTime::compare($thistime, $othertime) == 0);
	} elsif ($operator eq "lt") {
		$result = (DateTime::compare($thistime, $othertime) == -1);
	} elsif ($operator eq "gt") {
		$result = (DateTime::compare($thistime, $othertime) == 1);
	} elsif ($operator eq "leq") {
		$result = not (DateTime::compare($thistime, $othertime) == 1);
	} elsif ($operator eq "geq") {
		$result = not (DateTime::compare($thistime, $othertime) == -1);
	} elsif ($operator eq "neq") {
		$result = not (DateTime::compare($thistime, $othertime) == 0);
	}

	if ($result) {
		return IkiWiki::SuccessReason->new("Test is true");
	} else {
		return IkiWiki::FailReason->new("Test is false");
	}
}

{
	no strict "refs";
	my @creationmodificationlist = qw(c m);
	my @datetimelist = qw(date time);
	my @operatorslist = qw(lt leq gt geq eq neq);
	my @objlist = qw(page abs now today);
	my @deltalist = ("", "_delta");
	foreach my $cm (@creationmodificationlist) {
		foreach my $datetime (@datetimelist) {
			foreach my $operator (@operatorslist) {
				foreach my $obj (@objlist) {
					foreach my $delta (@deltalist) {
						my $name = "${cm}${datetime}_${operator}_${obj}${delta}";
						*{"match_$name"} = sub {
							return compare($cm, $datetime, $operator, $obj, $delta, @_);
						}
					}
				}
			}
		}
	}
}

1
