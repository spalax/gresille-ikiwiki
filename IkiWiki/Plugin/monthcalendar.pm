#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::monthcalendar;

=head1 NAME

IkiWiki::Plugin::monthcalendar - IkiWiki plugin to display big month calendars.

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::monthcalendar

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

Big month calendar, with days listing all related pages.
To be used as a template by ikiwiki-calendar. Provide a nicer (in my opinion)
page than the default inline.

See doc/plugins/monthcalendar.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/monthcalendar
http://ikiwiki.info/plugins/contrib/monthcalendar/

=head1 AUTHOR

Manoj Srivastava wrote the original Ikiwiki::plugin::calendar plugin.
Louis Paternault (spalax) <spalax at gresille dot org> improved it to make this plugin.

=head1 COPYRIGHT

Copyright (c) 2006, 2007 Manoj Srivastava <srivasta at debian dot org>
Copyright (c) 2012-2013 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut


use warnings;
use strict;
use IkiWiki 3.00;
use Time::Local;
use POSIX qw/setlocale LC_TIME strftime/;
use Encode;

my $time=time;
my @now=localtime($time);

sub import {
	hook(type => "getsetup", id => "monthcalendar", call => \&getsetup);
	hook(type => "format", id => "monthcalendar", call => \&format);
	hook(type => "preprocess", id => "monthcalendar", call => \&preprocess);
}

sub format (@) {
	# Insert javascript code
	my %params=@_;

	# Include css link right before </head>
	my $includecss = "<link rel=\"stylesheet\" href=\"" .  urlto("monthcalendar/monthcalendar.css", $params{page}) . "\" type=\"text/css\" />\n";
	$params{content}=~s!^(</head[^>]*>)!$includecss.$1!em;

	return $params{content};
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "widget",
		},
		archivebase => {
			type => "string",
			example => "archives",
			description => "base of the archives hierarchy",
			safe => 1,
			rebuild => 1,
		},
		archive_pagespec => {
			type => "pagespec",
			example => "page(posts/*) and !*/Discussion",
			description => "PageSpec of pages to include in the archives; used by ikiwiki-calendar command",
			link => 'ikiwiki/PageSpec',
			safe => 1,
			rebuild => 0,
		},
		week_start_day => {
			type => "integer",
			example => 1,
			description => "first day of the week (0 is Sunday)",
			safe => 1,
			rebuild => 1,
		},
}

sub is_leap_year (@) {
	my %params=@_;
	return ($params{year} % 4 == 0 && (($params{year} % 100 != 0) || $params{year} % 400 == 0));
}

sub month_days {
	my %params=@_;
	my $days_in_month = (31,28,31,30,31,30,31,31,30,31,30,31)[$params{month}-1];
	if ($params{month} == 2 && is_leap_year(%params)) {
		$days_in_month++;
	}
	return $days_in_month;
}

sub format_month (@) {
	my %params=@_;

	my %linkcache;
	foreach my $p (pagespec_match_list($params{page}, 
				"creation_year($params{year}) and creation_month($params{month}) and ($params{pages})",
				# add presence dependencies to update
				# month calendar when pages are added/removed
				deptype => deptype("presence"))) {
		my $mtime = $IkiWiki::pagectime{$p};
		my @date  = localtime($mtime);
		my $mday  = $date[3];
		my $month = $date[4] + 1;
		my $year  = $date[5] + 1900;
		my $mtag  = sprintf("%02d", $month);

		if (! $linkcache{"$year/$mtag/$mday"}) {
			$linkcache{"$year/$mtag/$mday"} = [];
		}
		push(@{$linkcache{"$year/$mtag/$mday"}}, $p);
	}
		
	my $pmonth = $params{month} - 1;
	my $nmonth = $params{month} + 1;
	my $pyear  = $params{year};
	my $nyear  = $params{year};

	# Adjust for January and December
	if ($params{month} == 1) {
		$pmonth = 12;
		$pyear--;
	}
	if ($params{month} == 12) {
		$nmonth = 1;
		$nyear++;
	}

	# Add padding.
	$pmonth=sprintf("%02d", $pmonth);
	$nmonth=sprintf("%02d", $nmonth);

	my $calendar="\n";

	# When did this month start?
	my @monthstart = localtime(timelocal(0,0,0,1,$params{month}-1,$params{year}-1900));

	my $future_dom = 0;
	my $today      = 0;
	if ($params{year} == $now[5]+1900 && $params{month} == $now[4]+1) {
		$future_dom = $now[3]+1;
		$today      = $now[3];
	}

	# Find out month names for this, next, and previous months
	my $monthname=strftime_utf8("%B", @monthstart);
	my $pmonthname=strftime_utf8("%B", localtime(timelocal(0,0,0,1,$pmonth-1,$pyear-1900)));
	my $nmonthname=strftime_utf8("%B", localtime(timelocal(0,0,0,1,$nmonth-1,$nyear-1900)));

	my $archivebase = 'archives';
	$archivebase = $config{archivebase} if defined $config{archivebase};
	$archivebase = $params{archivebase} if defined $params{archivebase};
  
	# Calculate URL's for monthly archives.
	my ($current, $pyurl, $pmurl, $nmurl, $nyurl)=("$monthname $params{year}",'','','','');
	# Current month
	if (exists $pagesources{"$archivebase/$params{year}"}) {
		$current = "$monthname " . htmllink($params{page}, $params{destpage}, 
			"$archivebase/$params{year}",
			noimageinline => 1,
			linktext => "$params{year}",
			title => $monthname);
	}
	add_depends($params{page}, "$archivebase/$params{year}/$params{month}",
		deptype("presence"));
	# Previous year
	my $pypage = sprintf("$archivebase/%04d/$params{month}", ($params{year} - 1));
	if (exists $pagesources{$pypage}) {
		$pyurl = htmllink($params{page}, $params{destpage}, 
			$pypage,
			noimageinline => 1,
			linktext => "\&larr;",
			title => "$monthname " . ($params{year} - 1));
	}
	add_depends($params{page}, $pypage,
		deptype("presence"));
	# Previous month
	if (exists $pagesources{"$archivebase/$pyear/$pmonth"}) {
		$pmurl = htmllink($params{page}, $params{destpage}, 
			"$archivebase/$pyear/$pmonth",
			noimageinline => 1,
			linktext => "\&larr;",
			title => "$pmonthname $pyear");
	}
	add_depends($params{page}, "$archivebase/$pyear/$pmonth",
		deptype("presence"));
	# Next month
	if (exists $pagesources{"$archivebase/$nyear/$nmonth"}) {
		$nmurl = htmllink($params{page}, $params{destpage}, 
			"$archivebase/$nyear/$nmonth",
			noimageinline => 1,
			linktext => "\&rarr;",
			title => "$nmonthname $nyear");
	}
	add_depends($params{page}, "$archivebase/$nyear/$nmonth",
		deptype("presence"));
	# Next year
	my $nypage = sprintf("$archivebase/%04d/$params{month}", ($params{year} + 1));
	if (exists $pagesources{$nypage}) {
		$nyurl = htmllink($params{page}, $params{destpage}, 
			$nypage,
			noimageinline => 1,
			linktext => "\&rarr;",
			title => "$monthname " . ($params{year} + 1));
	}
	add_depends($params{page}, $nypage,
		deptype("presence"));

	# Start producing the month calendar
	$calendar=<<EOF;
<table class="monthcalendar">
	<tr>
	<th class="monthcalendar-arrow">$pyurl</th>
	<th class="monthcalendar-arrow">$pmurl</th>
	<th class="monthcalendar-head" colspan="3">$current</th>
	<th class="monthcalendar-arrow">$nmurl</th>
	<th class="monthcalendar-arrow">$nyurl</th>
	</tr>
	<tr>
EOF
	# List of pages
	my $pagelist = "";

	# Suppose we want to start the week with day $week_start_day
	# If $monthstart[6] == 1
	my $week_start_day = 0;
	$week_start_day = $config{week_start_day} if defined $config{week_start_day};

	my $start_day = 1 + (7 - $monthstart[6] + $week_start_day) % 7;
	my %downame;
	for my $dow ($week_start_day..$week_start_day+6) {
		my @day=localtime(timelocal(0,0,0,$start_day++,$params{month}-1,$params{year}-1900));
		my $downame = strftime_utf8("%A", @day);
		$downame{$dow % 7}=$downame;
		$calendar.= qq{\t\t<th class="monthcalendar-day-head $downame" title="$downame">$downame</th>\n};
	}

	$calendar.=<<EOF;
	</tr>
EOF

	my $wday;
	# we start with a week_start_day, and skip until we get to the first
	for ($wday=$week_start_day; $wday != $monthstart[6]; $wday++, $wday %= 7) {
		$calendar.=qq{\t<tr>\n} if $wday == $week_start_day;
		$calendar .= qq{\t\t<td class="monthcalendar-day-noday $downame{$wday}">&nbsp;</td>\n};
	}

	# At this point, either the first is a week_start_day, in which case
	# nothing has been printed, or else we are in the middle of a row.
	for (my $day = 1; $day <= month_days(year => $params{year}, month => $params{month});
	     $day++, $wday++, $wday %= 7) {
		# At this point, on a week_start_day, we close out a row,
		# and start a new one -- unless it is week_start_day on the
		# first, where we do not close a row -- since none was started.
		if ($wday == $week_start_day) {
			$calendar.=qq{\t</tr>\n} unless $day == 1;
			$calendar.=qq{\t<tr>\n};
		}
		
		my $tag;
		my $key="$params{year}/$params{month}/$day";
		if (defined $linkcache{$key}) {
			if ($params{today} && $day == $today) {
				$tag='monthcalendar-day-this-day';
			}
			else {
				$tag='monthcalendar-day-link';
			}
			$calendar.=qq{\t\t<td class="$tag $downame{$wday}"><div><span class='monthcalendar-daynumber'>$day</span>};
			$calendar.=qq{\t\t\t<ul>};
			my $iterator = 0;
			foreach my $page (@{$linkcache{$key}}) {
				my $anchor = $params{year}.$params{month}.$day;
				if (scalar(@{$linkcache{$key}}) != 1) {
					$anchor .= chr(97 + $iterator);
					$iterator += 1;
				}
				$calendar.= "<li><a href=\#$anchor>";
				if (exists $pagestate{$page}{meta}{title}) {
					$calendar.= "$pagestate{$page}{meta}{title}";
				} else {
					$calendar.= IkiWiki::basename($page);
				}
				$calendar.= '</a></li>';
				IkiWiki::loadplugin("inline");
				$pagelist .= "<span id=$anchor class='monthcalendar-item'><a name=$anchor></a>";
				$pagelist .= IkiWiki::preprocess_inline(
					pagenames => "$page",
					page => $params{page},
					destpage => $params{destpage},
					template => "monthcalendar_item",
					feeds => "no") . "\n";
				$pagelist .= "</span>";
			}
			$calendar.=qq{</ul>};
			$calendar.=qq{</div></td>\n};
		}
		else {
			if ($params{today} && $day == $today) {
				$tag='monthcalendar-day-this-day';
			}
			elsif ($params{today} && $day == $future_dom) {
				$tag='monthcalendar-day-future';
			}
			else {
				$tag='monthcalendar-day-nolink';
			}
			$calendar.=qq{\t\t<td class="$tag $downame{$wday}"><div><span class='monthcalendar-daynumber'>$day</span></div></td>\n};
		}
	}

	# finish off the week
	for (; $wday != $week_start_day; $wday++, $wday %= 7) {
		$calendar.=qq{\t\t<td class="monthcalendar-day-noday $downame{$wday}">&nbsp;</td>\n};
	}
	$calendar.=<<EOF;
	</tr>
</table>
EOF

	return $calendar . "\n" . "<div class=\"monthcalendar-pagelist\">" . $pagelist . "</div>";
}

sub format_year (@) {
	# I wrote this function for future improvements. But right now, I do not plan
	# to fill it.
	error("Not implemented. Will be implemented when I need it, or when you submit a patch ;)");
}

sub preprocess (@) {
	my %params=@_;

	my $thisyear=1900 + $now[5];
	my $thismonth=1 + $now[4];

	$params{pages} = "*"            unless defined $params{pages};
	$params{type}  = "month"        unless defined $params{type};
	$params{today}  = "yes"         unless defined $params{type};
	$params{today} = IkiWiki::yesno($params{today});
	$params{week_start_day} = 0     unless defined $params{week_start_day};
	$params{year}  = $thisyear	unless defined $params{year};
	$params{month} = $thismonth	unless defined $params{month};

	my $relativeyear=0;
	if ($params{year} < 1) {
		$relativeyear=1;
		$params{year}=$thisyear+$params{year};
	}
	my $relativemonth=0;
	if ($params{month} < 1) {
		$relativemonth=1;
		my $monthoff=$params{month};
		$params{month}=($thismonth+$monthoff) % 12;
		$params{month}=12 if $params{month}==0;
		my $yearoff=POSIX::ceil(($thismonth-$params{month}) / -12)
			- int($monthoff / 12);
		$params{year}-=$yearoff;
	}
	
	$params{month} = sprintf("%02d", $params{month});
	
	my $calendar="";
	if ($params{type} eq 'month') {
		$calendar=format_month(%params);
	}
	elsif ($params{type} eq 'year') {
		$calendar=format_year(%params);
	}

	return "\n<div>$calendar</div>\n";
}

1
