#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::poetry;

=head1 NAME

IkiWiki::Plugin::poetry - Typeset poetry

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::poetry

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

See doc/plugins/poetry.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/poetry
http://ikiwiki.info/plugins/contrib/poetry/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "getsetup", id => "poetry", call => \&getsetup);
	hook(type => "preprocess", id => "poetry", call => \&preprocess);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "widget",
		},
}

sub preprocess (@) {

	my %params=@_;
	$params{content} = ""            unless defined $params{content};
  my $result = "";

  $result .= "<div class='poetry'>\n";

  foreach my $stanza (split /\n\s*\n/, $params{content}) {
    my $paren = 0;
    if ($stanza =~ /^\)\s/) {
      $result .= "<span class='paren'>\n";
      $stanza =~ s{(^|(?<=\n))\)\s*}//g;
      $paren = 1;
    }
    if ($stanza =~ /^>\s/) {
      $result .= "<p class='chorus'>\n";
      $stanza =~ s{(^|(?<=\n))>\s*}//g;
    } else {
      $result .= "<p class='stanza'>\n";
    }

    foreach my $line (split /^/, $stanza) {
      $result .= '<span class="line">' . $line . "</span>\n";
    }

    $result .= "</p>";
    if ($paren) {
      $result .= "</span>\n";
    }
  }

  $result .= "</div>\n";

  return $result;
}

1
