#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::parenttag;

=head1 NAME

IkiWiki::Plugin::parenttag - Also tag pages using parent tags.

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::parenttag

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

Whenever a page is tagged "some/tag/with/directories", automatically marks it
as "some", "some/tag" and "some/tag/with" as well.

See doc/plugins/parenttag.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/parenttag
http://ikiwiki.info/plugins/contrib/parenttag/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2014 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

use warnings;
use strict;
use IkiWiki 3.00;

my $orig_preprocess_tag;
my $orig_preprocess_taglink;

sub import {
	IkiWiki::loadplugin("tag");
	$orig_preprocess_tag = \&{$IkiWiki::hooks{preprocess}{tag}{call}};
	$orig_preprocess_taglink = \&{$IkiWiki::hooks{preprocess}{taglink}{call}};
	hook(type => "preprocess", id => "tag", call => \&my_preprocess_tag, scan => 1);
	hook(type => "preprocess", id => "taglink", call => \&my_preprocess_taglink, scan => 1);
}

sub my_preprocess_tag(@) {
	if (! @_) {
		return "";
	}
	my %params=@_;
	foreach my $tag (keys %params) {
		if (not ($tag eq "page" or $tag eq "destpage" or $tag eq "preview")) {
			my $prefix = "";
			foreach my $chunk (split('/', $tag)) {
				if ($prefix) {
					$prefix .= "/";
				}
				$prefix .= $chunk;
				$orig_preprocess_tag->(
					page => $params{page},
					destpage => $params{destpage},
					preview => $params{preview},
					$prefix => ''
				);
			}
		}
 }
 return "";
}

sub my_preprocess_taglink(@) {
	if (! @_) {
		return "";
	}
	my %params=@_;
	my $out = "";
	foreach my $tag (keys %params) {
		$out .= " ";
		my $tagout = "";
		if (not ($tag eq "page" or $tag eq "destpage" or $tag eq "preview")) {
			my $prefix = "";
			my $title = "";
			if ($tag =~ /(.*)\|(.*)/) {
				$tag = $2;
				$title = $1;
			}
			foreach my $chunk (split('/', $tag)) {
				if ($prefix) {
					$prefix .= "/";
					$tagout .= "/";
				}
				$prefix .= $chunk;
				$tagout .= $orig_preprocess_taglink->(
					page => $params{page},
					destpage => $params{destpage},
					preview => $params{preview},
					$prefix => ''
				);
			}
			if ($title) {
				$out .= $title;
			} else {
				$out .= $tagout;
			}
		}
	}
	return $out;
}

1
