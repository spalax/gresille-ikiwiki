#! /usr/bin/perl
require 5.002;
package IkiWiki::Plugin::taskreport;

=head1 NAME

IkiWiki::Plugin::taskreport - Display taskwarrior report

=head1 VERSION

This describes version B<0.1> of IkiWiki::Plugin::taskreport

=cut

our $VERSION = '0.1';

=head1 DESCRIPTION

Display taskwarrior report, as a table.

See taskwarrior website: http://taskwarrior.org

See doc/plugins/taskreport.mdwn for documentation.

=head1 PREREQUISITES

IkiWiki
JSON

Binary: task (version 2)

=head1 URL

https://spalax.frama.io/gresille-ikiwiki/taskreport
http://ikiwiki.info/plugins/contrib/taskreport/

=head1 AUTHOR

Louis Paternault (spalax) <spalax at gresille dot org>

=head1 COPYRIGHT

Copyright (c) 2013 Louis Paternault <spalax at gresille dot org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

=cut

use warnings;
use strict;
use IkiWiki 3.00;
use DateTime::Format::ISO8601;
use File::Path;
use File::Copy;
use File::Spec;

sub import {
	hook(type => "getsetup", id => "task", call => \&getsetup);
	hook(type => "checkconfig", id => "task", call => \&checkconfig);
	hook(type => "preprocess", id => "task", call => \&preprocess);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => undef,
			section => "widget",
		},
		task_bin => {
			type => "string",
			example => "/usr/bin/task",
			description => "Binary to use to call Taskwarrior.",
			safe => 0,
			rebuild => 1,
		},
		task_common => {
			type => "string",
			example => "rc:DIR/TO/taskrc",
			description => "List of arguments to be passed to all task calls.",
			safe => 1,
			rebuild => 1,
		},
		task_columns => {
			type => "string",
			example => "qw(due description)",
			description => "List of colums to show.",
			safe => 1,
			rebuild => 1,
		},
		task_dir => {
			type => "string",
			example => "/home/ikiwiki/wiki/task",
			description => "Directory of task data files, if not default (~/.task) and  handled by IkiWiki.",
			safe => 1,
			rebuild => 1,
		},
		task_tmpdir => {
			type => "string",
			example => "/home/ikiwiki/tmp/task",
			description => "Directory where to copy task data files, to circumvent lack of option '--read-only': http://taskwarrior.org/issues/424",
			safe => 0,
			rebuild => 1,
		},
}

my %fields = (
	urgency => "Float",
	status => "String",
	uuid => "UUID",
	entry => "Date",
	description => "String",
	start => "Date",
	end => "Date",
	due => "Date",
	until => "Date",
	wait => "Date",
	recur => "String",
	mask => "String",
	imask => "Integer",
	parent => "UUID",
	entry => "Date",
	description => "String",
	project => "String",
	tags => "Array",
	priority => "String",
	depends => "String",
);

sub checkconfig() {
	$config{task_bin} = "task"   unless defined $config{task_bin};
	$config{task_common} = ""    unless defined $config{task_common};
	@{$config{task_columns}} = keys(%fields) unless defined $config{task_columns};

	# Give a nice error if the (non-standard) JSON module is not installed.
	eval "use JSON";
	if ($@) {
		$config{task_error} = ("You need to install the JSON Perl module.");
	}

	# Use the taskwarrior 2.0+ export command to filter and return JSON
	my @command = (
		$config{task_bin},
		"rc.verbose=nothing",
		$config{task_common},
	);
	
	if (defined($config{task_dir})) {
		if (defined($config{task_tmpdir})) {
			# Workaround while waiting for option "--readonly" for task.
			# http://bug.tasktools.org/browse/TW-204
			if (not -d $config{task_tmpdir}) {
				File::Path::make_path($config{task_tmpdir}, {error => \my $err});
				if (@$err) {
					$config{task_error} = "Cannot create path \"$config{task_tmpdir}\".";
				}
			}
			foreach my $file (qw(pending undo completed)) {
				unless (File::Copy::copy(
					srcfile(File::Spec->catfile($config{task_dir}, "$file.data")),
					File::Spec->catfile($config{task_tmpdir}, "$file.data"),
				)) {
					$config{task_error} = (
						"Cannot copy \""
						. File::Spec->catfile($config{task_dir}, "$file.data")
						. "\" to \""
						. File::Spec->catfile($config{task_tmpdir}, "$file.data")
						. "\": $!"
					);
				}
			}
			push(@command, "rc.data.location=$config{task_tmpdir}");
		} else {
			push(@command, "rc.data.location=$config{task_dir}");
		}
	}
	@{$config{task_basecommand}} = @command;
}

sub date_format(@) {
	return DateTime::Format::ISO8601->parse_datetime($_[0])->strftime("%F");
}

sub preprocess (@) {
	if ($config{task_error}) {
		error($config{task_error});
	}

	my %params=@_;
	$params{arg} = ""            unless defined $params{arg};
	$params{show} = 0            unless defined $params{show};
	$params{sort} = "urgency"    unless defined $params{sort};
	$params{reverse} = "no"      unless defined $params{reverse};
	$params{annotations} = "yes" unless defined $params{annotations};

	if (defined($config{task_dir})) {
		foreach my $file (qw(pending undo completed)) {
			add_depends($params{page}, "$config{task_dir}/$file.data");
		}
	}

	my @columns;
	if (defined($params{columns})) {
		@columns = split(/ +/, $params{columns});
	} else {
		@columns = @{$config{task_columns}};
	}

	my @command = @{$config{task_basecommand}};
	push(@command, $params{arg});
	push(@command, "export");

	my $stdout = qx(@command);
	$stdout =~ s/^\[?/[/;
	$stdout =~ s/\]?$/]/;

	if (($? >> 8) != 0) {
		error("Command returned non-zero status : @command.");
	}
	my @tasklist = from_json($stdout, {utf8=>1})->[0];

	unless(exists($tasklist[0]{$params{sort}})) {
		error("Invalid sort key '$params{sort}'. Command and result were:\n\n"
	. "    @command\n\n"
	. "    " . join("\n    ", split('\n', $stdout)));
	}
	sub mysort($$$$) {
		my ($a, $b, $reverse, $key) = @_;
		my $result = 0;
		if ($key eq "urgency") {
			$result =  ($a->{$key} <=> $b->{$key});
		} else {
			$result =  ($a->{$key} cmp $b->{$key});
		}
		if (IkiWiki::yesno($reverse)) {
			return -$result;
		} else {
			return $result;
		}
	}
	@tasklist = sort {mysort($a, $b, $params{reverse}, $params{sort})} (@tasklist);

	my $show = $params{show};
	if ($show == 0) {
		$show = -1;
	}

	my $result .= "<table class='task'><thead><tr>";
	foreach my $key (@columns) {
		$result .= "<th class='$key'>$key</th>";
	}
	$result .= "</tr></thead>";

	for my $data ($tasklist[0])
	{

		if ($show == 0) {
			last;
		}
		$show -= 1;

		$result .= "<tbody><tr>";
		my $annottable = "";

		foreach my $key (@columns) {
			$result .= "<td class='$key'>";
			if (exists $data->{$key}) {
				if (not defined $fields{$key}) {
					$result .= " ";
				} elsif ($fields{$key} eq "Date") {
					$result .= date_format($data->{$key});
				} elsif ( $fields{$key} eq "Array") {
					$result .= join (',', @{$data->{$key}});
				} else {
					$result .= $data->{$key};
				}
			}
			$result .= "</td>";
		}


		# Add annotations
		if (exists $data->{'annotations'})
		{
			for my $annotation (@{$data->{'annotations'}}) {
				$annottable .= "<tr class='annotation'>";
				$annottable .= "<td></td>";
				$annottable .= "<td>" . date_format($annotation->{'entry'}) . "</td>";
				$annottable .= "<td colspan='0'>$annotation->{'description'}</td>";
				$annottable .= "</tr>\n";
			}
		}

		$result .= "</tr>\n";
		if (IkiWiki::yesno($params{annotations})) {
			$result .= $annottable;
		}
		$result .= "</tbody>\n";
	}

	$result .= "</table>";

	return $result;
}

1
