[[!meta title="Duckies are fun!"]]
[[!meta date="2012-08-02 18:30:00"]]

As you know, birds do not have sexual organs because they would interfere
with flight.  [In fact, this was the big breakthrough for the Wright
Brothers.  They were watching birds one day, trying to figure out how to get
their crude machine to fly, when suddenly it dawned on Wilbur.  "Orville,"
he said, "all we have to do is remove the sexual organs!" You should have
seen their original design.] As a result, birds are very, very difficult to
arouse sexually.  You almost never see an aroused bird.  So when they want
to reproduce, birds fly up and stand on telephone lines, where they monitor
telephone conversations with their feet.  When they find a conversation in
which people are talking dirty, they grip the line very tightly until they
are both highly aroused, at which point the female gets pregnant.
		-- Dave Barry, "Sex and the Single Amoeba: What Every
		   Teen Should Know"
