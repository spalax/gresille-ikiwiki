[[!meta title="Must be over 18."]]
[[!meta date="2012-11-15 18:30:00"]]

I don't care if it rains or freezes
Long as I've got my plastic jesus
Sitting on the dashboard of my car
Comes in colors pink and pleasant
Glows in the dark cause it's iridescent
Take it with you when you travel far.

Get yourself a sweet madonna
Dressed in rhinestones sitting on a
Pedestal of abalone shell
Going ninety I aint scary
Cause I've got the virgin mary
Telling me that I won't go to hell.
    [Paul Newman, in "Cool Hand Luke"]
