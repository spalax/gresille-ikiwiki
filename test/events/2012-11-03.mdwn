[[!meta title="May contain nuts."]]
[[!meta date="2012-11-03 18:30:00"]]

If a jury in a criminal trial stays out for more than twenty-four hours, it
is certain to vote acquittal, save in those instances where it votes guilty.
		-- Joseph C. Goulden
