[[!meta title="Yow!"]]
[[!meta date="2012-10-16 18:30:00"]]

There is no point in waiting.
The train stopped running years ago.
All the schedules, the brochures,
The bright-colored posters full of lies,
Promise rides to a distant country
That no longer exists.
