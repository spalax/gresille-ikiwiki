# Copyright Louis Paternault 2015
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>. 1

"""IkiWiki markdown README renderer"""

import os
from evariste.plugins.renderer.html.readme import HtmlRenderer
from proxy import IkiWikiProcedureProxy
ikiproxy = IkiWikiProcedureProxy(__name__, debug_fn=None)

class MdwnRenderer(HtmlRenderer):
    """Markdown renderer for readme files."""

    keyword = "renderer.html.readme.mdwn"
    extensions = ["mdwn"]

    def render(self, path, context):
        """Render file as html code.
        """
        destpage = self.local.setup['page']
        page = os.path.relpath(
            os.path.join(
                context['tree'].from_fs.as_posix(),
                path,
            ),
            ikiproxy.rpc("getvar", "config", "srcdir")
            )
        with open(path) as source:
            return ikiproxy.rpc(
                "htmlize", page, destpage, "mdwn",
                ikiproxy.rpc(
                    "linkify", page, destpage,
                    ikiproxy.rpc(
                        "preprocess", page, destpage,
                        ikiproxy.rpc(
                            "filter", page, destpage, source.read()
                            )
                        )
                    )
                )
